/*
 * LCD.h
 *
 *  Created on: Dec 6, 2017
 *      Author: Zealot
 */

#ifndef LCD_LCD_H_
#define LCD_LCD_H_

#include <stdint.h>
#include "F2806x_Device.h"
 /****************************************************************************
 * USER DEFINITIONS
******************************************************************************/

 /****************************************************************************
 * HARD DEFINITIONS
******************************************************************************/

/* Display ON/OFF Control definitions */
#define DON          0x0F  /* Display on      */
#define DOFF         0x0B  /* Display off     */
#define CURSOR_ON    0x0F  /* Cursor on       */
#define CURSOR_OFF   0x0D  /* Cursor off      */
#define BLINK_ON     0x0F  /* Cursor Blink    */
#define BLINK_OFF    0x0E  /* Cursor No Blink */

/* Cursor or Display Shift definitions */
#define SHIFT_CUR_LEFT     0x04  /* Cursor shifts to the left   */
#define SHIFT_CUR_RIGHT    0x05  /* Cursor shifts to the right  */
#define SHIFT_DISP_LEFT    0x06  /* Display shifts to the left  */
#define SHIFT_DISP_RIGHT   0x07  /* Display shifts to the right */

/* Function Set definitions */
#define FOUR_BIT    0x2C  /* 4-bit Interface               */
#define EIGHT_BIT   0x3C  /* 8-bit Interface               */
#define LINE_5X7    0x30  /* 5x7 characters, single line   */
#define LINE_5X10   0x34  /* 5x10 characters               */
#define LINES_5X7   0x38  /* 5x7 characters, multiple line */

 /****************************************************************************
* FUNCTIONS 'S PROTOTYPES
******************************************************************************/
// For further description, see LCD.c
void lcd_delay_us (Uint32 t);
void lcd_delay_ms (Uint32 t);
void lcd_put_byte(Uint8 rs, Uint8 data);

void lcd_init(void);
void lcd_clear(void);
void lcd_gotoxy(Uint8 col, Uint8 row);
void lcd_putc(char c);
void lcd_puts(const char* s);
void lcd_clr_puts (const char* s1 , const char* s2);
void lcd_put_num (Uint32 val, char dec, Uint8 neg);

void delay_ms();

#endif /* LCD_LCD_H_ */
